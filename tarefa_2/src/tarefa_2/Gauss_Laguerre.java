package tarefa_2;

public class Gauss_Laguerre {
	
	int pontos;
	
	public Gauss_Laguerre(int pontos) {
		this.pontos = pontos;
	}
	
	public double calcular() {
		
		double integral = 0.0;
		double[] e = new double[pontos]; 
		double[] w = new double[pontos];
		
		switch (pontos) {
		case 2:
			
			e[0] = 0.58578643;
			w[0] = 0.85355339;

			e[1] = 3.41421356;
			w[1] = 0.14644660;
			
			break;
			
		case 3:
			
			e[0] = 0.41577455;
			w[0] = 0.71109300;

			e[1] = 2.24428036;
			w[1] = 0.27851973;

			e[2] = 6.28994508;
			w[2] = 0.01038926;
			
			break;
			
		case 4:
			
			e[0] = 0.32254768;
			w[0] = 0.60315410;

			e[1] = 1.74576110;
			w[1] = 0.35741869;

			e[2] = 4.53662029;
			w[2] = 0.03888791;

			e[3] = 9.39507091;
			w[3] = 0.00053929;
			
			break;

		default:
			break;
		}
		
		for(int c = 0 ; c < pontos; c++)
			integral += funcao(e[c]) * w[c];
		
		return integral;
	}
	
	double funcao(double x) {
		
		//funcao e^(-x) cosseno(x)
		return Math.pow(Math.E, -x) * Math.cos(x);
	}
}
