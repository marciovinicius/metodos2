package tarefa_2;

public class Gauss_Hermite {
	
	int pontos;
	
	public Gauss_Hermite(int pontos) {
		this.pontos = pontos;
	}
	
	public double calcular() {
		
		double integral = 0.0;
		double[] e = new double[pontos]; 
		double[] w = new double[pontos];
		
		switch (pontos) {
		case 2:
			
			e[0] = 0.70710678;
			w[0] = 0.88622692;

			e[1] = -0.70710678;
			w[1] = 0.88622692;
			
			break;
			
		case 3:
			
			e[0] = 0;
			w[0] = 1.18163590;

			e[1] = 1.22474487;
			w[1] = 0.29540897;

			e[2] = -1.22474487;
			w[2] = 0.29540897;
			
			break;
			
		case 4:
			
			e[0] = 0.52464762;
			w[0] = 0.80491409;

			e[1] = -0.52464762;
			w[1] = 0.80491409;

			e[2] = 1.65068012;
			w[2] = 0.08131283;

			e[3] = -1.65068012;
			w[3] = 0.08131283;
			
			break;

		default:
			break;
		}
		
		for(int c = 0 ; c < pontos; c++)
			integral += funcao(e[c]) * w[c];
		
		return integral;
	}
	
	double funcao(double x) {
		
		//funcao e^(-x^2) cosseno(x)
		return Math.pow(Math.E, Math.pow(-x, 2)) * Math.cos(x);
	}
}
