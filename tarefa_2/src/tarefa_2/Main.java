package tarefa_2;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		double integral = 0.0;
		int pontos = 0;
		
		System.out.println( "Bem-vindo ao programa de Integra��o num�rica utilizando os metodos de Gauss! \n\n");
		
		int tipoQuadratura = 0;
		while (tipoQuadratura  < 1 || tipoQuadratura > 3) {
		   System.out.println( "Escolha o m�todo de integra��o. \n\n");
		   System.out.println( "1 - Gauss-Hermite. \n");
		   System.out.println( "2 - Gauss-Laguerre. \n");
		   System.out.println( "3 - Gauss-Chebyshev. \n");
		   tipoQuadratura = entrada.nextInt();
	    }
		
		if(tipoQuadratura == 1) {
			
			System.out.println( "Voc� escolheu o metodo Gauss-Hermite. \n");
			
			while(pontos  < 2 || pontos > 4 ) {
				System.out.println( "Escolha o numero de pontos de Hermite (2-4)\n");
				pontos = entrada.nextInt();
			}
			
			integral = new Gauss_Hermite(pontos).calcular();
			
			System.out.println("A integral da fun��o e^(-x^2)*cosseno(x) para Gauss-Hermite com "+pontos+" pontos �: "+integral);
			
		} else if(tipoQuadratura == 2) {
			
			System.out.println( "Voc� escolheu o metodo Gauss-Laguerre. \n");
			
			while(pontos  < 2 || pontos > 4 ) {
				System.out.println( "Escolha o numero de pontos de Laguerre (2-4)\n");
				pontos = entrada.nextInt();
			}
			
			integral = new Gauss_Laguerre(pontos).calcular();
			
			System.out.println("A integral da fun��o e^(-x)*cosseno(x) para Gauss-Laguerre com "+pontos+" pontos �: "+integral);		
		} else if(tipoQuadratura == 3) {
			
			System.out.println( "Voc� escolheu o metodo Gauss-Chebyshev. \n");
			
			while(pontos  < 1 ) {
				System.out.println( "Escolha o numero de pontos N de Chebyshev (valor inteiro)\n");
				pontos = entrada.nextInt();
			}
			
			integral = new Gauss_Chebyshev(pontos).calcular();
			
			System.out.println("A integral da fun��o 1/raiz(1-x^2)*cosseno(x) para Gauss-Chebyshev com N="+pontos+" �: "+integral);
			
		}
	}

}
