package tarefa_2;

public class Gauss_Chebyshev {
	
	int pontos;
	public Gauss_Chebyshev(int pontos) {
		
		this.pontos = pontos;
	}

	public double calcular() {
	
		double integral = 0.0;
		
		for(int ponto = 0 ; ponto < pontos; ponto++)
			integral += funcao(Math.cos(((ponto - 0.5)/pontos) * Math.PI));
		
		return (Math.PI/pontos)*integral;
	}

	private double funcao(double x) {
		
		// 1/raiz(x^2) * cosseno(x)
		return 1/(Math.sqrt(1-Math.pow(x, 2))) * Math.cos(x);
	}

}
