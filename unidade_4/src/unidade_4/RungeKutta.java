package unidade_4;

public class RungeKutta {

	public double rungeKutta(double n, double deltaT, Funcao f, double valorInicial, int ordem) {

		double yn = valorInicial;

		switch (ordem) {
		case 2:
			for (double ponto = deltaT; ponto < n; ponto += deltaT) {
				yn = yn + rungeKuttaSegundaOrdem(yn, ponto, f, deltaT);
			}
			break;

		case 3:
			for (double ponto = deltaT; ponto < n; ponto += deltaT) {
				yn = yn + rungeKuttaTerceiraOrdem(yn, ponto, f, deltaT);
			}
			break;
		case 4:
			for (double ponto = deltaT; ponto < n; ponto += deltaT) {
				yn = yn + rungeKuttaQuartaOrdem(yn, ponto, f, deltaT);
			}
			break;

		default:
			break;
		}

		return yn;
	}

	private double rungeKuttaQuartaOrdem(double yn, double ponto, Funcao f, double deltaT) {
		double k1 = deltaT * f.resultado(yn, ponto);
		double k2 = deltaT * f.resultado(yn + k1 / 3, ponto + deltaT / 3);
		double k3 = deltaT * f.resultado(yn + k2 / 2, ponto + 2 * deltaT / 3);
		double k4 = deltaT * f.resultado(yn + k1 - k2 + k3, ponto + deltaT);
		return (k1 + 3 * k2 + 3 * k3 + k4) / 8;
	}

	private double rungeKuttaTerceiraOrdem(double yn, double ponto, Funcao f, double deltaT) {
		double k1 = deltaT * f.resultado(yn, ponto);
		double k2 = deltaT * f.resultado(yn + k1 / 2, ponto + deltaT / 2);
		double k3 = deltaT * f.resultado(yn - k1 + 2 * k2, ponto + deltaT);
		return (k1 + 4 * k2 + k3) / 6;
	}

	private double rungeKuttaSegundaOrdem(double yn, double ponto, Funcao f, double deltaT) {
		double k1 = deltaT * f.resultado(yn, ponto);
		double k2 = deltaT * f.resultado(yn + k1, ponto + deltaT);
		return (k1 + k2) / 2;
	}
}
