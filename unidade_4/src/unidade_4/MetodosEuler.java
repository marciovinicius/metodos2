package unidade_4;

public class MetodosEuler {

	public double forwardEuler(double n, Funcao f, double deltaT, double valorInicial) {

		int tamanho = (int) Math.floor(n / deltaT) + 1;
		double[] y = new double[tamanho];
		y[0] = valorInicial;
		double tempoAgora = 0.0;

		for (int i = 1; i < n; i++) {
			y[i] = y[i - 1] + deltaT * f.resultado(y[i - 1], tempoAgora);
			tempoAgora += deltaT;
		}

		return y[tamanho];
	}

	public double backwardEuler(double n, Funcao f, double deltaT, double valorInicial, double erro, double chute) {

		int tamanho = (int) Math.floor(n / deltaT) + 1;
		double[] y = new double[tamanho];
		y[0] = valorInicial;
		double tempoAgora = 0.0 + deltaT;

		for (int i = 1; i < n; i++) {
			double dif;
			double valor = y[i - 1] + chute;
			do {
				y[i] = y[i - 1] + deltaT * f.resultado(valor, tempoAgora);
				dif = Math.abs(y[i] - valor);
				valor = y[i];
			} while (dif > erro);

			tempoAgora += deltaT;
		}

		return y[tamanho];
	}
}
