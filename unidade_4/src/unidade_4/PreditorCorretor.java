package unidade_4;

public class PreditorCorretor {

	public double preditorCorretor(double n, double valorInicial, Funcao f, double deltaT, int pontos, double erro) {

		MetodosEuler me = new MetodosEuler();
		int tamanho = (int) Math.floor(n / deltaT) + 1;
		double[] y = new double[tamanho];
		y[0] = valorInicial;

		for (int i = 1; i < pontos; i++) {
			double ponto = i * deltaT;
			y[i] = me.forwardEuler(ponto, f, deltaT, valorInicial);
		}

		switch (pontos) {
		case 2:
			for (int i = 2; i < tamanho; i++) {
				// preditor
				y[i] = y[i - 1] + (deltaT / 2)
						* (3 * f.resultado(y[i - 1], (i - 1) * deltaT) - f.resultado(y[i - 2], (i - 2) * deltaT));

				// corretor
				double dif;
				do {
					double corretor = y[i - 1]
							+ (deltaT / 2) * (f.resultado(y[i], i * deltaT) + f.resultado(y[i - 1], (i - 1) * deltaT));
					dif = Math.abs(corretor - y[i]);
					y[i] = corretor;
				} while (dif > erro);
			}
			break;

		case 3:
			for (int i = 3; i < tamanho; i++) {
				// preditor
				y[i] = y[i - 1] + (deltaT / 12) * (23 * f.resultado(y[i - 1], (i - 1) * deltaT)
						- 16 * f.resultado(y[i - 2], (i - 2) * deltaT) + 5 * f.resultado(y[i - 3], (i - 3) * deltaT));

				// corretor
				double dif;
				do {
					double corretor = y[i - 1] + (deltaT / 12) * (5 * f.resultado(y[i], i * deltaT)
							+ 8 * f.resultado(y[i - 1], (i - 1) * deltaT) - f.resultado(y[i - 2], (i - 2) * deltaT));
					dif = Math.abs(corretor - y[i]);
					y[i] = corretor;
				} while (dif > erro);
			}

		case 4:
			for (int i = 4; i < tamanho; i++) {
				// preditor
				y[i] = y[i - 1] + (deltaT / 24) * (55 * f.resultado(y[i - 1], (i - 1) * deltaT)
						- 59 * f.resultado(y[i - 2], (i - 2) * deltaT) + 37 * f.resultado(y[i - 3], (i - 3) * deltaT)
						- 9 * f.resultado(y[i - 4], (i - 4) * deltaT));

				// corretor
				double dif;
				do {
					double corretor = y[i - 1] + (deltaT / 24) * (9 * f.resultado(y[i], i * deltaT)
							+ 19 * f.resultado(y[i - 1], (i - 1) * deltaT) - 5 * f.resultado(y[i - 2], (i - 2) * deltaT)
							+ f.resultado(y[i - 3], (i - 3) * deltaT));
					dif = Math.abs(corretor - y[i]);
					y[i] = corretor;
				} while (dif > erro);
			}

		case 5:
			for (int i = 5; i < tamanho; i++) {
				// preditor
				y[i] = y[i - 1] + (deltaT / 720) * (1901 * f.resultado(y[i - 1], (i - 1) * deltaT)
						- 2774 * f.resultado(y[i - 2], (i - 2) * deltaT)
						+ 2616 * f.resultado(y[i - 3], (i - 3) * deltaT)
						- 1274 * f.resultado(y[i - 4], (i - 4) * deltaT)
						+ 251 * f.resultado(y[i - 5], (i - 5) * deltaT));

				// corretor
				double dif;
				do {
					double corretor = y[i - 1] + (deltaT / 720)
							* (251 * f.resultado(y[i], i * deltaT) + 646 * f.resultado(y[i - 1], (i - 1) * deltaT)
									- 264 * f.resultado(y[i - 2], (i - 2) * deltaT)
									+ 106 * f.resultado(y[i - 3], (i - 3) * deltaT)
									- 19 * f.resultado(y[i - 4], (i - 4) * deltaT));
					dif = Math.abs(corretor - y[i]);
					y[i] = corretor;
				} while (dif > erro);
			}

		default:
			break;
		}

		return y[tamanho];
	}
}
