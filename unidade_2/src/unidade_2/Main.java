package unidade_2;

public class Main {

	public static void main(String[] args) {

		double deltaX = 1.0 / 13.0;
		double valor = derivadaParcial(new Funcao(), deltaX, 1.0, 0.5);

		System.out.println(valor);
	}

	public static double derivadaParcial(Funcao f, double deltaX, double x, double y) {

		return (-f.resultado(x + 2 * deltaX, y) + 8 * f.resultado(x + deltaX, y) - 8 * f.resultado(x - deltaX, y)
				+ f.resultado(x - 2 * deltaX, y)) / (12.0 * deltaX);
	}
}
