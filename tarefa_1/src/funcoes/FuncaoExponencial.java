package funcoes;

public class FuncaoExponencial extends Funcao{

	@Override
	public double f(double x) {
		// TODO Auto-generated method stub
		return Math.pow(Math.E, x) / Math.sqrt(1-x*x);
	}

}
