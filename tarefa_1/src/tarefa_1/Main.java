package tarefa_1;

import java.util.Scanner;
import funcoes.Funcao;
import funcoes.FuncaoCosseno;
import funcoes.FuncaoCubica;
import funcoes.FuncaoQuadratica;
import funcoes.FuncaoSeno;
import funcoes.FuncaoTangente;
import integracoes.Integracao;
import integracoes.*;

public class Main {

	public static void main(String[] args) {		
		
		Scanner entrada = new Scanner(System.in);
		
		int tipoQuadratura =  0;
		int nc_fechada_ou_aberta = 0;
		int grau_polinomio_substituicao = -1;
		int gauss_tipo =  0;
		int particao_ou_precisao = 0;
		int numero_de_particoes = 0;
		int id_integrando = 1;
		Funcao integrando = null;
		Integracao pintegrObj;
		
		double a = -1.0;
		double b = 1.0;
		double precisao = 0.0001;
		double integral = 0;
		
		System.out.println( "Bem-vindo ao programa de Integra��o num�rica! \n\n");
		   
	   System.out.println( "Escolha a fun��o a ser integrada: \n");
	   System.out.println( "\t1 - x^2 \n");
	   System.out.println( "\t2 - x^3 \n");
	   System.out.println( "\t3 - cos(x) \n");
	   System.out.println( "\t4 - sin(x) \n");
	   System.out.println( "\t5 - tan(x) \n");
	   
	   id_integrando = entrada.nextInt();
	   
	   switch (id_integrando)
	    {
	        case 1:
	            integrando = new FuncaoQuadratica();
	            break;
	        case 2:
	            integrando = new FuncaoCubica();
	            break;
	        case 3:
	            integrando = new FuncaoCosseno();
	            break;
	        case 4:
	            integrando = new FuncaoSeno();
	            break;
	        case 5:
	            integrando = new FuncaoTangente();
	            break;
	    }
	   
	   System.out.println( "Forne�a os limites de intragra��o x_min e x_max: \n");
	   System.out.println( "x_min = " ); 
	   a = entrada.nextDouble();
	   System.out.println( "\nx_max = " );
	   b = entrada.nextDouble();
	   
	   while(particao_ou_precisao < 1 || particao_ou_precisao > 2)
       {
           System.out.println( "Voc� deseja particionar o intervalo de integra��o (a, b) ou quer definir uma precis�o? \n");
           System.out.println( "1 - Particionar.                     \n");
           System.out.println( "2 - Definir a precis�o do resultado. \n");
           particao_ou_precisao = entrada.nextInt();
       }
	   
	   if (particao_ou_precisao == 1)
       {
		   System.out.println( "\n\nEntre o n�mero de parti��es do intervalo (a,b) - n�mero >= 1. \n");
           numero_de_particoes = entrada.nextInt();
       } else {
    	   System.out.println( "\n\nEntre o valor da toler�ncia epslon (exemplo: 0.00001)\n");
           precisao = entrada.nextDouble();
       }
	   
	   while (tipoQuadratura < 1 || tipoQuadratura > 3)
	    {
		   System.out.println( "Escolha o m�todo de integra��o. \n\n");
		   System.out.println( "1 - Newton-Cotes Fechada. \n");
		   System.out.println( "2 - Newton-Cotes Aberta. \n");
		   System.out.println( "3 - Gauss-Legendre. \n");
		   tipoQuadratura = entrada.nextInt();
	    }
	   
	   if(tipoQuadratura == 1) {
		   System.out.println("Voc� escolheu Newton-Cotes Fechada. \n");
		   
		   while (grau_polinomio_substituicao < 1 || grau_polinomio_substituicao > 4)
           {
               System.out.println( "Escolha o grau do polin�mio de substitui��o (1-4)\n\n");
               grau_polinomio_substituicao = entrada.nextInt();
           }
		   
		   switch (grau_polinomio_substituicao)
           {
               case 1:
                   pintegrObj = new NC_F_P1(integrando, a, b, numero_de_particoes, precisao);
                   integral   = pintegrObj.integrar();
                   break;

               case 2:
                   pintegrObj = new NC_F_P2(integrando, a, b, numero_de_particoes, precisao);
                   integral   = pintegrObj.integrar();
                   break;

               case 3:
                   pintegrObj = new NC_F_P3(integrando, a, b, numero_de_particoes, precisao);
                   integral   = pintegrObj.integrar();
                   break;

               case 4:
                   pintegrObj = new NC_F_P4(integrando, a, b, numero_de_particoes, precisao);
                   integral   = pintegrObj.integrar();
                   break;
           } 
	   } else if(tipoQuadratura == 2) {
		   System.out.println("Voc� escolheu Newton-Cotes Aberta. \n");
		   
		   while (grau_polinomio_substituicao < 1 || grau_polinomio_substituicao > 4)
           {
               System.out.println( "Escolha o grau do polin�mio de substitui��o (1-4)\n\n");
               grau_polinomio_substituicao = entrada.nextInt();
           }
		   
		   switch (grau_polinomio_substituicao)
           {
               case 1:
                   pintegrObj = new NC_A_P1(integrando, a, b, numero_de_particoes, precisao);
                   integral   = pintegrObj.integrar();
                   break;

               case 2:
                   pintegrObj = new NC_A_P2(integrando, a, b, numero_de_particoes, precisao);
                   integral   = pintegrObj.integrar();
                   break;

               case 3:
                   pintegrObj = new NC_A_P3(integrando, a, b, numero_de_particoes, precisao);
                   integral   = pintegrObj.integrar();
                   break;

               case 4:
                   pintegrObj = new NC_A_P4(integrando, a, b, numero_de_particoes, precisao);
                   integral   = pintegrObj.integrar();
                   break;
           } 
       } else if(tipoQuadratura == 3) {
          System.out.println("Voc� escolheu Gauss-Legendre. \n");
		   
		   while (grau_polinomio_substituicao < 2 || grau_polinomio_substituicao > 5) {
               System.out.println( "Escolha o numero de pontos de Legendre (2-5)\n\n");
               grau_polinomio_substituicao = entrada.nextInt();
           }
		   
		   switch (grau_polinomio_substituicao){
               case 2:
                   pintegrObj = new GL_P2(integrando, a, b, numero_de_particoes, precisao);
                   integral   = pintegrObj.integrar();
                   break;

               case 3:
                   pintegrObj = new GL_P3(integrando, a, b, numero_de_particoes, precisao);
                   integral   = pintegrObj.integrar();
                   break;

               case 4:
                   pintegrObj = new GL_P4(integrando, a, b, numero_de_particoes, precisao);
                   integral   = pintegrObj.integrar();
                   break;

               case 5:
                   pintegrObj = new GL_P5(integrando, a, b, numero_de_particoes, precisao);
                   integral   = pintegrObj.integrar();
                   break;
           } 
   	   }
	   
	   System.out.println( "A integral da fun��o escolhida, no intervalo (" +a + ", " + b + ") � : ");
	   System.out.println( integral+"\n\n" );
	}
}
