package integracoes;

import funcoes.Funcao;

public class GL_P3 extends Integracao {
	
	Funcao m_pIntegrando;
    double m_lInf;
    double m_lSup;
    double m_precisao;
    int m_particao;
	
	public GL_P3(Funcao f, double a, double b, int N, double epsilon){
		m_pIntegrando = f;
	    m_lInf        = a;
	    m_lSup        = b;
	    m_precisao    = epsilon;
	    m_particao    = N;
	}
	
	@Override
	public double integrar() {
		double integral = 0.0;

	    if (m_particao >= 1)
	    {
	        double step = (m_lSup - m_lInf)/m_particao;
	        double linf, lsup;

	        for (int i = 0; i < m_particao; i++)
	        {
	            linf = m_lInf + i*step;
	            lsup = linf   + step;

	            integral += GL_P3_integracao(linf, lsup);
	        }

	        return integral;
	    }
	    else
	    {
	        int    N           = 1;
	        double oldintegral = 0.0;
	        double step;
	        double linf, lsup;

	        integral = GL_P3_integracao(m_lInf, m_lSup);

	        do
	        {
	            oldintegral = integral;
	            integral    = 0.0;

	            N = N*2;

	            step = (m_lSup - m_lInf)/N;

	            for (int i = 0; i < N; i++)
	            {
	                linf = m_lInf + i*step;
	                lsup = linf   + step;

	                integral += GL_P3_integracao(linf, lsup);
	            }


	        } while (Math.abs(integral - oldintegral) > m_precisao);

	        return integral;
	    }
	}

	private double GL_P3_integracao( double a, double b) {
		int pontos = 3;
		double[] e = new double[pontos]; double[] w = new double[pontos];
		e[0] = -0.7745966692;
		w[0] = 0.5555555555;
		
		e[1] = 0;
		w[1] = 0.8888888888;
		
		e[2] = 0.7745966692;
		w[2] = 0.5555555555;
		
		double integral = 0.0;
		
		for(int c = 0 ; c < pontos; c++)
			 integral += calcularPontos(a,b,e[c]) * w[c];
		
		return integral * (b-a)/2 ;
	}

	private double calcularPontos(double a, double b, double e) {
		
		double x = ((a+b)/2) + (e*( (b-a)/2 ));
		return m_pIntegrando.f(x);
	}

}
