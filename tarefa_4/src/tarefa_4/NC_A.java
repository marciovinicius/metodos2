package tarefa_4;

public class NC_A {
	
	public double integrar(Funcao f, double a, double b, double x, int grau) {
		
		double integral = 0.0;
		double h = 0;
		
		switch (grau) {
		case 1:
			h = (b - a)/3.0;
			integral = (((b-a)/2.0)*(f.calcular(x,a+h) + f.calcular(x,a+2*h)));
			break;
			
		case 2:
			h = (b - a)/4.0;
			integral = ((4.0*h/3)*(2.0*f.calcular(x,a+h) - f.calcular(x,a+2*h) + 2.0*f.calcular(x,a+3*h)));
			break;
			
		case 3:
			h = (b - a)/5.0;
			integral = ((5.0*h/24)*(11.0*f.calcular(x,a+h) + f.calcular(x,a+2*h) + f.calcular(x,a+3*h) + 11.0*f.calcular(x,a+4*h)));
			break;
	
		case 4:
			h = (b - a)/6.0;
			integral = ((6.0*h/20)*(11.0*f.calcular(x,a+h) - 14*f.calcular(x,a+2*h) + 26*f.calcular(x,a+3*h) - 14.0*f.calcular(x,a+4*h) + 11.0*f.calcular(x,a+5*h)));
			break;
		}
		
		return integral;
	}
}
