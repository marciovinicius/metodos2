package tarefa_4;

public class GL {

	Funcao f = new Funcao();
	
	public double integrar(double a, double b, int pontos, double x ) {
		
		double integral = 0.0;
		double[] e = new double[pontos]; double[] w = new double[pontos];
		
		switch (pontos) {
		case 2:
			e[0] = -0.5773502691; 
			w[0] = 1;
			
			e[1] = 0.5773502691;
			w[1] = 1;
			
			break;
			
		case 3:
			e[0] = -0.7745966692;
			w[0] = 0.5555555555;
			
			e[1] = 0;
			w[1] = 0.8888888888;
			
			e[2] = 0.7745966692;
			w[2] = 0.5555555555;
			
			break;
		case 4:
			e[0] = -0.8611363115;
			w[0] = 0.3478548451;
			
			e[1] = -0.3399810435;
			w[1] = 0.6521451548;
			
			e[2] = 0.3399810435;
			w[2] = 0.6521451548;
			
			e[3] = 0.8611363115;
			w[3] = 0.3478548451;
			
			break;
		case 5:
			e[0] = -0.9061798459;
			w[0] = 0.2369268850;
			
			e[1] = -0.5384693101;
			w[1] = 0.4786286704;
			
			e[2] = 0;
			w[2] = 0.5688888888;
			
			e[3] = 0.5384693101;
			w[3] = 0.4786286704;
			
			e[4] = 0.9061798459;
			w[4] = 0.2369268850;
			
			break;

		default:
			break;
		}
		
		for(int c = 0 ; c < pontos; c++)
			 integral += calcularPontos(a,b,e[c],x) * w[c];
		
		return integral * (b-a)/2 ;
	}
	
	private double calcularPontos(double a, double b, double e, double x) {
		
		double y = ((a+b)/2) + (e*( (b-a)/2 ));
		return f.calcular(x, y);
	}
}
