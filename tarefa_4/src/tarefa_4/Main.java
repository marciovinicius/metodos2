package tarefa_4;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		double integral = 0.0;
		double h = 0;
		Funcao f = new Funcao();
		NC_F ncf = new NC_F();
		NC_A nca = new NC_A();
		GL gl = new GL();
		
		System.out.println("Forne�a g1(x)\n");
		double g1x = entrada.nextDouble();
		System.out.println("Forne�a g2(x)\n");
		double g2x = entrada.nextDouble();
		System.out.println("Forne�a a\n");
		double a = entrada.nextDouble();
		System.out.println("Forne�a b\n");
		double b = entrada.nextDouble();
		
		System.out.println("Escolha o metodo para integra��o em Y\n");
		System.out.println( "1 - Newton-Cotes Fechada. \n");
		System.out.println( "2 - Newton-Cotes Aberta. \n");
		System.out.println( "3 - Gauss-Legendre. \n");
		int metodo = entrada.nextInt();
		
		System.out.println("Escolha o grau de substitui��o (1-4) ou o numero de pontos, caso seja Gauss-Legendre (2-5)\n");
		int grau = entrada.nextInt();
		
		if(metodo == 1) {		
			switch (grau) {
			case 1:
				integral = (((b-a)/2)*(ncf.integrar(f, g1x, g2x, a, grau)  + ncf.integrar(f, g1x, g2x, b, grau)));
				break;
				
			case 2:
				h = (b - a)/2;
				integral = (h/3*(ncf.integrar(f, g1x, g2x, a, grau) + 4.0*ncf.integrar(f, g1x, g2x, a+h, grau) + ncf.integrar(f, g1x, g2x, b, grau)));
				break;
				
			case 3:
				h = (b - a)/3;
				integral = ((3*h/8)*(ncf.integrar(f, g1x, g2x, a, grau) + 3.0*ncf.integrar(f, g1x, g2x, a+h, grau) + 3.0*ncf.integrar(f, g1x, g2x, a+2*h, grau) + ncf.integrar(f, g1x, g2x, b, grau)));
				break;
		
			case 4:
				h = (b - a)/4;
				integral = ((2*h/45)*(7*ncf.integrar(f, g1x, g2x, a, grau) + 32.0*ncf.integrar(f, g1x, g2x, a+h, grau) + 12.0*ncf.integrar(f, g1x, g2x, a+2*h, grau) + 32.0*ncf.integrar(f, g1x, g2x, a+3*h, grau) + 7*ncf.integrar(f, g1x, g2x, b, grau)));
				break;
			}
		} else if(metodo == 2) {
			switch (grau) {
			case 1:
				h = (b - a)/3.0;
				integral = (((b-a)/2.0)*(nca.integrar(f, g1x, g2x, a+h, grau) + nca.integrar(f, g1x, g2x, a+2*h, grau)));
				break;
				
			case 2:
				h = (b - a)/4.0;
				integral = ((4.0*h/3)*(2.0*nca.integrar(f, g1x, g2x, a+h, grau) - nca.integrar(f, g1x, g2x, a+2*h, grau) + 2.0*nca.integrar(f, g1x, g2x, a+3*h, grau)));
				break;
				
			case 3:
				h = (b - a)/5.0;
				integral = ((5.0*h/24)*(11.0*nca.integrar(f, g1x, g2x, a+h, grau) + nca.integrar(f, g1x, g2x, a+2*h, grau) + nca.integrar(f, g1x, g2x, a+3*h, grau) + 11.0*nca.integrar(f, g1x, g2x, a+4*h, grau)));
				break;
		
			case 4:
				h = (b - a)/6.0;
				integral = ((6.0*h/20)*(11.0*nca.integrar(f, g1x, g2x, a+h, grau) - 14*nca.integrar(f, g1x, g2x, a+2*h, grau) + 26*nca.integrar(f, g1x, g2x, a+3*h, grau) - 14.0*nca.integrar(f, g1x, g2x, a+4*h, grau) + 11.0*nca.integrar(f, g1x, g2x, a+5*h, grau)));
				break;
			}
		} else if(metodo == 3) {
			double[] e = new double[grau]; double[] w = new double[grau];
			
			switch (grau) {
			case 2:
				e[0] = -0.5773502691; 
				w[0] = 1;
				
				e[1] = 0.5773502691;
				w[1] = 1;
				
				break;
				
			case 3:
				e[0] = -0.7745966692;
				w[0] = 0.5555555555;
				
				e[1] = 0;
				w[1] = 0.8888888888;
				
				e[2] = 0.7745966692;
				w[2] = 0.5555555555;
				
				break;
			case 4:
				e[0] = -0.8611363115;
				w[0] = 0.3478548451;
				
				e[1] = -0.3399810435;
				w[1] = 0.6521451548;
				
				e[2] = 0.3399810435;
				w[2] = 0.6521451548;
				
				e[3] = 0.8611363115;
				w[3] = 0.3478548451;
				
				break;
			case 5:
				e[0] = -0.9061798459;
				w[0] = 0.2369268850;
				
				e[1] = -0.5384693101;
				w[1] = 0.4786286704;
				
				e[2] = 0;
				w[2] = 0.5688888888;
				
				e[3] = 0.5384693101;
				w[3] = 0.4786286704;
				
				e[4] = 0.9061798459;
				w[4] = 0.2369268850;
				
				break;

			default:
				break;
			}
			
			for(int c = 0 ; c < grau; c++)
				 integral += calcularPontos(a,b,e[c],g1x,g2x,grau) * w[c];
			
			integral =  integral * (b-a)/2 ;
		}
		
		System.out.println("Resultado = "+integral);
	}

	private static double calcularPontos(double a, double b, double e, double g1x, double g2x, int grau) {
		double x = ((a+b)/2) + (e*( (b-a)/2 ));
		return new GL().integrar(g1x, g2x, grau, x);
	}
}
