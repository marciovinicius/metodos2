package tarefa_4;

public class NC_F {
	
	public double integrar(Funcao f, double a, double b, double x, int grau) {
		
		double integral = 0.0;
		double h = 0;
		
		switch (grau) {
		case 1:
			integral = (((b-a)/2)*(f.calcular(x, a) + f.calcular(x,  b)));
			break;
			
		case 2:
			h = (b - a)/2;
			integral = (h/3*(f.calcular(x,a) + 4.0*f.calcular(x,a+h) + f.calcular(x,b)));
			break;
			
		case 3:
			h = (b - a)/3;
			integral = ((3*h/8)*(f.calcular(x,a) + 3.0*f.calcular(x,a+h) + 3.0*f.calcular(x,a+2*h) + f.calcular(x,b)));
			break;
	
		case 4:
			h = (b - a)/4;
			integral = ((2*h/45)*(7*f.calcular(x,a) + 32.0*f.calcular(x,a+h) + 12.0*f.calcular(x,a+2*h) + 32.0*f.calcular(x,a+3*h) + 7*f.calcular(x,b)));
			break;
		}
		
		return integral;
	}
}
