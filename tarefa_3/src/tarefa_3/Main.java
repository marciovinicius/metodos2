package tarefa_3;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		double integral = 0.0;
		double a = 0;
		double b = 0;
		
		System.out.println( "Bem-vindo ao programa de m�todos de integra��o exponenciais! \n\n");
		
		System.out.println( "Forne�a os limites de intragra��o x_min e x_max: \n");
		System.out.println( "x_min = " ); 
		a = entrada.nextDouble();
		System.out.println( "\nx_max = " );
		b = entrada.nextDouble();
		   
		System.out.println("Escolha o metodo de integra��o\n");
		System.out.println( "1 - Exponencial Simples. \n");
		System.out.println( "2 - Exponencial Dupla. \n");
		int exponencial = entrada.nextInt();
		
		System.out.println("Forne�a o valor de epsilon para a convergencia dos pontos de corte\n");
		double epsilon_1 = entrada.nextDouble();
		
		System.out.println("Forne�a o valor de epsilon para a convergencia da integral das fun��o\n");
		double epsilon_2 = entrada.nextDouble();
		
		System.out.println("Escolha o metodo de integra��o para calcular a integral da fun��o entre os pontos de corte\n");
		System.out.println( "1 - Newton-Cotes Fechada. \n");
		System.out.println( "2 - Newton-Cotes Aberta. \n");
		System.out.println( "3 - Gauss-Legendre. \n");
		int metodoIntegracao = entrada.nextInt();
		
		System.out.println("Escolha o grau de substitui��o (1-4) ou o numero de pontos caso seja Gauss-Legendre (2-5)\n");
		int grau = entrada.nextInt();
		
		integral = Exponencial.calcular(a, b, exponencial, epsilon_1, epsilon_2, metodoIntegracao, grau);
		
		System.out.println("Resultado = "+integral);
	}
}
