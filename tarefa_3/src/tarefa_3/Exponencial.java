package tarefa_3;

import funcoes.Funcao;
import funcoes.FuncaoExponencial;
import integracoes.*;

public class Exponencial {

	public static double calcular(double a, double b, int exponencial, double epsilon_1, double epsilon_2,
			int metodoIntegracao, int grau) {
		
		double integral = 0.0;
		double integralOld = 0.0;
		double currentError = 0.0;
		Integracao pintegrObj;
		Funcao f = new FuncaoExponencial();
		
		do {
			
			if(metodoIntegracao == 1) {
				switch (grau) {
		               case 1:
		                   pintegrObj = new NC_F_P1(f, a, b, 0, epsilon_2);
		                   integral   = pintegrObj.integrar();
		                   break;

		               case 2:
		                   pintegrObj = new NC_F_P2(f, a, b, 0, epsilon_2);
		                   integral   = pintegrObj.integrar();
		                   break;

		               case 3:
		                   pintegrObj = new NC_F_P3(f, a, b, 0, epsilon_2);
		                   integral   = pintegrObj.integrar();
		                   break;

		               case 4:
		                   pintegrObj = new NC_F_P4(f, a, b, 0, epsilon_2);
		                   integral   = pintegrObj.integrar();
		                   break;
		           } 
			} else if(metodoIntegracao == 2) {
				switch (grau) {
	               case 1:
	                   pintegrObj = new NC_A_P1(f, a, b, 0, epsilon_2);
	                   integral   = pintegrObj.integrar();
	                   break;

	               case 2:
	                   pintegrObj = new NC_A_P2(f, a, b, 0, epsilon_2);
	                   integral   = pintegrObj.integrar();
	                   break;

	               case 3:
	                   pintegrObj = new NC_A_P3(f, a, b, 0, epsilon_2);
	                   integral   = pintegrObj.integrar();
	                   break;

	               case 4:
	                   pintegrObj = new NC_A_P4(f, a, b, 0, epsilon_2);
	                   integral   = pintegrObj.integrar();
	                   break;
	           } 
			} else if(metodoIntegracao == 3) {
				
				switch (grau){
	               case 2:
	                   pintegrObj = new GL_P2(f, a, b, 0, epsilon_2);
	                   integral   = pintegrObj.integrar();
	                   break;

	               case 3:
	                   pintegrObj = new GL_P3(f, a, b, 0, epsilon_2);
	                   integral   = pintegrObj.integrar();
	                   break;

	               case 4:
	                   pintegrObj = new GL_P4(f, a, b, 0, epsilon_2);
	                   integral   = pintegrObj.integrar();
	                   break;

	               case 5:
	                   pintegrObj = new GL_P5(f, a, b, 0, epsilon_2);
	                   integral   = pintegrObj.integrar();
	                   break;
	           } 
			}
			
			currentError = Math.abs(integral - integralOld);
			integralOld = integral;
			a -= 1;
			b += 1;
		}while(currentError > epsilon_1);
		
		return integral;
	}
}
