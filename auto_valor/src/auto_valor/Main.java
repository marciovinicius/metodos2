package auto_valor;

public class Main {

	public static void main(String[] args) {

		Matriz A = new Matriz(3);
		A.setValor(0, 0, 32);
		A.setValor(0, 1, 5);
		A.setValor(0, 2, 2);

		A.setValor(1, 0, 5);
		A.setValor(1, 1, 23);
		A.setValor(1, 2, 4);

		A.setValor(2, 0, 2);
		A.setValor(2, 1, 4);
		A.setValor(2, 2, 59);
		
		Potencia p = new Potencia();
		double[] q = new double[A.size()];
		for (int i = 0; i < q.length; i++) {
			q[i] = 1;
		}
		
		Matriz AI = MatrizUtil.matrizInversa(A);
		
		p.potenciaRegular(A, q, 0.0001);
		System.out.println("Regular");
		System.out.println(p.autoValor);
		System.out.println(p.autoVetor[0]+" "+p.autoVetor[1]+" "+p.autoVetor[2]);

		p.potenciaInversa(A, q, 0.0001);
		System.out.println("Inversa");
		System.out.println(p.autoValor);
		System.out.println(p.autoVetor[0]+" "+p.autoVetor[1]+" "+p.autoVetor[2]);
		
		p.potenciaDeslocado(A, q, 40, 0.0001);
		System.out.println("Deslocado");
		System.out.println(p.autoValor);
		System.out.println(p.autoVetor[0]+" "+p.autoVetor[1]+" "+p.autoVetor[2]);
		
		A = metodoHQR(A);

		System.out.println("teste");
	}

	private static Matriz metodoHQR(Matriz A) {
		HouseHoulder h = new HouseHoulder();
		Matriz AT = h.calcularMatrizTridiagonal(A); // Matriz A Tridiagonal
		System.out.println("Matriz Tridiagonal");
		MatrizUtil.show(AT);
		Matriz H = h.getMatrizHouseHolder(); // Matriz H HouseHoulder
		System.out.println("Matriz H");
		MatrizUtil.show(H);
		
		/*Matriz HT = MatrizUtil.matrizTransposta(H);
		Matriz HTxA = MatrizUtil.multiplicarMatriz(HT, A);
		MatrizUtil.multiplicarMatriz(HTxA, H);*/
		
		QR qr = new QR();
		Matriz AD = qr.calcularMatrizDiagonal(AT); // Matriz A Diagonal
		System.out.println("Matriz Diagonal");
		MatrizUtil.show(AD);
		Matriz X = qr.X;
		System.out.println("Matriz X");
		MatrizUtil.show(X);

		// auto-valores
		for (int i = 0; i < AD.size(); i++) {
			A.setAutovalor(i, AD.getValor(i, i));
		}

		// auto-vetores
		Matriz AV = MatrizUtil.multiplicarMatriz(H, X);
		System.out.println("Matriz HX");
		MatrizUtil.show(AV);
		for (int j = 0; j < AV.size(); j++) {
			double[] autovetor = new double[AV.size()];
			for (int i = 0; i < AV.size(); i++) {
				autovetor[i] = AV.getValor(i, j);
			}
			A.setAutovetor(j, autovetor);
		}

		return A;
	}
}
