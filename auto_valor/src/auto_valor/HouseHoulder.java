package auto_valor;

public class HouseHoulder {
	
	Matriz H;

	public Matriz calcularMatrizTridiagonal(Matriz a) {
		
		int tamanho = a.size();
		H = MatrizUtil.matrizIdentidade(tamanho);
		
		for(int j=0; j<tamanho-2; j++) {
			double s = 0;
			double soma = 0;
			
			for(int i=j+1; i<tamanho; i++) {
				soma = soma + Math.pow(a.getValor(i, j), 2); 
			}
			
			s = Math.sqrt(soma);
			
			Matriz c = MatrizUtil.matrizZerada(tamanho);
			double cN = 0;
			for(int k=0; k<tamanho; k++) {
				if(k <= j) {
					c.setValor(k, j, 0);
				}else if(k == j+1) {
					double valor = a.getValor(k, j);
					if(valor >= 0)
						c.setValor(k, j, valor + s);
					else
						c.setValor(k, j, valor - s);
				}else {
					c.setValor(k, j, a.getValor(k, j));
				}
				cN = cN + Math.pow(c.getValor(k, j), 2);
			}
			cN = Math.sqrt(cN);
			
			Matriz e1 = MatrizUtil.dividirMatrizPorConstante(c, cN);
			Matriz e1T = MatrizUtil.matrizTransposta(e1);
			Matriz e1Xe1T = MatrizUtil.multiplicarMatriz(e1, e1T);
			Matriz e1Xe1TX2 = MatrizUtil.multiplicarMatrizPorConstante(e1Xe1T, 2);
			Matriz identidade = MatrizUtil.matrizIdentidade(tamanho);
			Matriz P = MatrizUtil.subtrairMatriz(identidade, e1Xe1TX2);
			Matriz PT = MatrizUtil.matrizTransposta(P);
			
			Matriz PxA = MatrizUtil.multiplicarMatriz(PT, a);
			a = MatrizUtil.multiplicarMatriz(PxA, P);
			
			H = MatrizUtil.multiplicarMatriz(H, PT);
		}
		
		return a;
	}
	
	public Matriz getMatrizHouseHolder() {
		return H;
	}
}
