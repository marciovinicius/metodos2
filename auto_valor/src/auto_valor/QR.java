package auto_valor;

public class QR {

	Matriz X;
	Matriz QT;

	public Matriz calcularMatrizDiagonal(Matriz a) {
		int tamanho = a.size();
		X = MatrizUtil.matrizIdentidade(tamanho);

		do {
			Matriz R = matrizR(a);
			Matriz Q = MatrizUtil.matrizTransposta(QT);
			a = MatrizUtil.multiplicarMatriz(R, Q);

			X = MatrizUtil.multiplicarMatriz(X, Q);
		} while (naoDiagonal(a));

		return a;
	}

	private boolean naoDiagonal(Matriz a) {
		int tamanho = a.size();
		boolean naoDiagonal = false;

		for (int i = 0; i < tamanho; i++) {
			for (int j = 0; j < tamanho; j++) {
				if (i != j) {
					double valor = a.getValor(i, j);
					if (Math.abs(valor) > 0.0001) {
						naoDiagonal = true;
						break;
					}
				}
			}
		}
		return naoDiagonal;
	}

	private Matriz matrizR(Matriz a) {
		int tamanho = a.size();
		QT = MatrizUtil.matrizIdentidade(tamanho);
		double Aij;
		double Ajj;
		double angulo;
		double sen;
		double cos;
		Matriz Ak = MatrizUtil.multiplicarMatriz(a, MatrizUtil.matrizIdentidade(tamanho));

		for (int j = 0; j < tamanho - 1; j++) {
				
			Matriz jacobiana = MatrizUtil.matrizIdentidade(tamanho);
			int i = j + 1;

			do {
				Aij = Ak.getValor(i, j);
				if (Math.abs(Aij) < 0.0001)
					continue;
				Ajj = Ak.getValor(j, j);
				if (Ajj == 0)
					angulo = 90;
				else
					angulo = Math.atan(Aij / Ajj);
				sen = Math.sin(angulo);
				cos = Math.cos(angulo);

				jacobiana.setValor(i, i, cos);
				jacobiana.setValor(i, j, -1 * sen);
				jacobiana.setValor(j, i, sen);
				jacobiana.setValor(j, j, cos);
				
				QT = MatrizUtil.multiplicarMatriz(jacobiana, QT);
				Ak =  MatrizUtil.multiplicarMatriz(jacobiana, Ak);
			} while(Ak.getValor(i, j) > 0.0001);
		}

		return Ak;
	}
}
