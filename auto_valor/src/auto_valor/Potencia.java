package auto_valor;

public class Potencia {

	MatrizUtil mu = new MatrizUtil();
	double autoValor;
	double[] autoVetor;

	public void potenciaRegular(Matriz A, double[] q, double erro) {
		
		double av;
		double avPassado = 0;
		double[] Z;
		double normaZ;
		double[] qTxA;
		double dif = 0;
		do{
			
			Z = mu.multiplicarMatrizVetor(A, q);
			normaZ = norma(Z);
			q = mu.dividirVetorConstante(Z, normaZ);
			
			qTxA = mu.multiplicarVetorMatriz(q, A);
			av = mu.multiplicarVetores(qTxA, q);
			
			dif = Math.abs(av - avPassado);
			avPassado = av;
		}while(dif > erro);
		
		autoValor = av;
		autoVetor = q;
	}
	
	public void potenciaInversa(Matriz A, double[] q, double erro) {
		Matriz inversa = mu.matrizInversa(A);
		potenciaRegular(inversa, q, erro);
		autoValor = 1 / autoValor;
	}
	
	public void potenciaDeslocado(Matriz A, double[] q, double deslocamento, double erro) {
		Matriz IxD = mu.multiplicarMatrizPorConstante(mu.matrizIdentidade(A.size()), deslocamento);
		Matriz AD = mu.subtrairMatriz(A, IxD);
		potenciaInversa(AD, q, erro);
		
		autoValor = autoValor + deslocamento;
	}

	public double norma(double[] v) {
		double resultado;

		double soma = 0;
		for (int i = 0; i < v.length; i++) {
			soma += Math.pow(v[i], 2);
		}

		resultado = Math.sqrt(soma);
		return resultado;
	}
}
