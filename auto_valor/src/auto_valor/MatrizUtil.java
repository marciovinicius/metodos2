package auto_valor;

public class MatrizUtil {

	public static Matriz multiplicarMatriz(Matriz a, Matriz b) {

		int tamanho = a.size();

		Matriz resultado = new Matriz(tamanho);
		double soma;

		for (int i = 0; i < tamanho; i++) {
			for (int j = 0; j < tamanho; j++) {
				soma = 0;
				for (int k = 0; k < tamanho; k++) {
					soma = soma + a.getValor(i, k) * b.getValor(k, j);
				}
				resultado.setValor(i, j, soma);
			}
		}
		return resultado;
	}

	public static Matriz matrizIdentidade(int tamanho) {

		Matriz identidade = new Matriz(tamanho);

		for (int i = 0; i < tamanho; i++) {
			for (int j = 0; j < tamanho; j++) {
				if (i == j)
					identidade.setValor(i, j, 1);
				else
					identidade.setValor(i, j, 0);
			}
		}
		return identidade;
	}

	public static Matriz matrizTransposta(Matriz a) {
		int tamanho = a.size();
		Matriz transposta = new Matriz(tamanho);

		for (int i = 0; i < tamanho; i++) {
			for (int j = 0; j < tamanho; j++) {
				double valor = a.getValor(i, j);
				transposta.setValor(j, i, valor);
			}
		}

		return transposta;
	}

	public static Matriz matrizZerada(int tamanho) {
		Matriz zerada = new Matriz(tamanho);

		for (int i = 0; i < tamanho; i++) {
			for (int j = 0; j < tamanho; j++) {
				zerada.setValor(i, j, 0);
			}
		}
		return zerada;
	}

	public static Matriz dividirMatrizPorConstante(Matriz a, double n) {
		int tamanho = a.size();
		Matriz resultante = new Matriz(tamanho);

		for (int i = 0; i < tamanho; i++) {
			for (int j = 0; j < tamanho; j++) {
				double valor = a.getValor(i, j);
				resultante.setValor(i, j, valor / n);
			}
		}
		return resultante;
	}

	public static Matriz multiplicarMatrizPorConstante(Matriz a, double n) {
		int tamanho = a.size();
		Matriz resultante = new Matriz(tamanho);

		for (int i = 0; i < tamanho; i++) {
			for (int j = 0; j < tamanho; j++) {
				double valor = a.getValor(i, j);
				resultante.setValor(i, j, valor * n);
			}
		}
		return resultante;
	}

	public static Matriz subtrairMatriz(Matriz a, Matriz b) {
		int tamanho = a.size();
		Matriz resultado = new Matriz(tamanho);

		for (int i = 0; i < tamanho; i++) {
			for (int j = 0; j < tamanho; j++) {
				double valorA = a.getValor(i, j);
				double valorB = b.getValor(i, j);
				resultado.setValor(i, j, valorA - valorB);
			}
		}
		return resultado;
	}

	public static double[] multiplicarMatrizVetor(Matriz m, double[] v) {
		double[] vetor = new double[v.length];

		for (int i = 0; i < m.size(); i++) {
			double soma = 0;
			for (int j = 0; j < m.size(); j++) {
				soma += m.getValor(i, j) * v[j];
			}
			vetor[i] = soma;
		}
		return vetor;
	}

	public double[] multiplicarVetorMatriz(double[] v, Matriz m) {
		double[] vetor = new double[v.length];

		for (int i = 0; i < v.length; i++) {
			double soma = 0;
			for (int j = 0; j < m.size(); j++) {
				soma += v[j] * m.getValor(j, i);
			}
			vetor[i] = soma;
		}
		return vetor;
	}

	public double[] dividirVetorConstante(double[] v, double c) {
		double[] vetor = new double[v.length];

		for (int i = 0; i < v.length; i++) {
			vetor[i] = v[i] / c;
		}

		return vetor;
	}

	public double multiplicarVetores(double[] v1, double[] v2) {

		double soma = 0;
		for (int i = 0; i < v1.length; i++) {
			soma += v1[i] * v2[i];
		}
		return soma;
	}

	public static Matriz matrizInversa(Matriz A) {
		Matriz inversa = MatrizUtil.matrizIdentidade(A.size());
		Matriz m = MatrizUtil.multiplicarMatriz(A, inversa);
		double valor;

		for (int j = 0; j < m.size(); j++) {
			valor = m.getValor(j, j);
			if (valor != 0) {
				for (int k = 0; k < m.size(); k++) {
					m.setValor(j, k, m.getValor(j, k) / valor);
					inversa.setValor(j, k, inversa.getValor(j, k) / valor);
				}
			}
			for (int i = 0; i < m.size(); i++) {
				valor = m.getValor(i, j);
				if (i != j && valor != 0) {
					double valorNegativo = valor * -1;
					for (int k = 0; k < m.size(); k++) {
						m.setValor(i, k, valorNegativo * m.getValor(j, k) + m.getValor(i, k));
						inversa.setValor(i, k, valorNegativo * inversa.getValor(j, k) + inversa.getValor(i, k));
					}
				}
			}
		}
		return inversa;
	}
	
	public static void show(Matriz A) {
		int tamanho = A.size();
		
		for (int i = 0; i < tamanho; i++) {
			System.out.println("\n");
			for (int j = 0; j < tamanho; j++) {
				System.out.print(A.getValor(i, j)+" ");
			}
		}
		System.out.println("\n");
	}
}
