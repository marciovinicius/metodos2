package auto_valor;

public class Matriz {

	private int tamanho;
	private double[][] matriz;
	private double[] autovalor;
	private double[][] autovetor;

	Matriz(int tamanho) {
		this.tamanho = tamanho;
		matriz = new double[tamanho][tamanho];
		autovalor = new double[tamanho];
		autovetor = new double[tamanho][tamanho];
	}

	public void setValor(int linha, int coluna, double valor) {
		matriz[linha][coluna] = valor;
	}

	public double getValor(int linha, int coluna) {
		return matriz[linha][coluna];
	}
	
	public void setAutovalor(int n, double valor) {
		autovalor[n] = valor;
	}
	
	public double getAutovalor(int n) {
		return autovalor[n];
	}
	
	public void setAutovetor(int n, double[] vetor) {		
		for(int i=0; i<tamanho; i++) {
			autovetor[n][i] = vetor[i];
		}
	}
	
	public double[] getAutovetor(int n) {
		double[] vetor = new double[tamanho];
		
		for(int i=0; i<tamanho; i++) {
			vetor[i] = autovetor[n][i];
		}
		return vetor;
	}
	
	public int size() {
		return tamanho;
	}
}
